#include <iostream>

struct List
{
    List* next;
    List* prev;
    int x;
};

void ADD(List* head, int x)
{
    List* p = new List;
    p->x = x;

    if (head->next == head->next->next)
    {
        p->next = head->next;
        p->prev = head->next;
        head->next->next = p;
        head->next->prev = p;
    }
    else
    {
        p->next = head->next;
        p->prev = head->next->prev;
        head->next->prev->next = p;
        head->next->prev = p;
    }
}

void Print(List* head)
{
    std::cout << std::endl;
    List* p = new List;
    p = head->next;

    std::cout << "Forward: ";
    do // ����� �����
    {
        std::cout << p->x << " ";
        p = p->next;
    } while (p != head->next);
    std::cout << std::endl;
    p = head->next;

    std::cout << "Backward: ";
    do // ����� �����
    {
        std::cout << p->x << " ";
        p = p->prev;
    } while (p != head->next);
}

void Insert(List* head, int x, int k)
{
    List* p = new List;
    p->x = x;

    if (head->next == head->next->next)
    {
        if (k == 0 || k == 1)
        {
            if (k == 0)
            {
                head->next = p;
                p->next = p;
                p->prev = p;
            }

            else
            {
                p->next = head->next;
                p->prev = head->next;
                head->next->next = p;
                head->next->prev = p;
            }
        }
    }

    else
    {
        int u = 0;
        List* r = new List;
        r = head->next;
        do
        {
            if (u == k)
            {
                p->next = r;
                p->prev = r->prev;
                r->prev->next = p;
                r->prev = p;
            }
            u++;
            r = r->next;
        } while (r != head->next);
    }
}

void Delete(List* head, int k)
{
    if (head->next == head->next->next)
    {
        if (k == 0)
        {
            delete head->next;
            head->next = nullptr;
        }
    }

    else
    {
        int u = 0;
        List* r = new List;
        r = head->next;
        do
        {
            if (u == k)
            {
                r->prev->next = r->next;
                r->next->prev = r->prev;
                delete r;
                break;
            }
            u++;
            r = r->next;
        } while (r != head->next);
    }
}

int main()
{
    List* head = new List;
    head->prev = head;
    head->next = head;

    std::cout << "Initial List:";
    ADD(head, 489);
    ADD(head, 576);
    ADD(head, 21);
    ADD(head, 3571);
    Print(head);

    std::cout << "\n\nInsert <0>:";
    Insert(head, 0, 2);
    Print(head);

    std::cout << "\n\nInsert <111>:";
    Insert(head, 111, 3);
    Print(head);

    std::cout << "\n\nDelete <second> element:";
    Delete(head, 1);
    Print(head);

    std::cout << std::endl;
    return 0;
}